Implantation of firebase 

Before implementing the firebase app, I was required to create an account through the creation of my account I was able to gain access to the platform after creating your account you're required to create the Web App which is be used for setting up firebase. 

First you navigate to the home page by clicking on console. 

Step two you click on add project 

Step three you give your project a name  

Step four you configure the features of your project 

And once you’re done following the procedure firebase will start creating your project 

Once the firebase web app has been successfully created, I clicked web app to generate the script tag which contains that I've copied onto my HTML page which will be used to implement a firebase. 

Once I was done copying my firebase code I then when back to the home page of firebase to create a database known as users this database will not be used to store information about a particular user, but it also serves a point key factor which I've used to implement my CRUD functions 

Implantation of firebase authentication  

Firebase has multiple features that users can use to incorporate into their systems one of them are the authentication methods such as Facebook, Twitter, Web/email password, and google this method of authentication from what I've seen is not only a gate way which can be used to protect your system and identify users but it's also a gateway which can be used to store information of new users that sign up to your system  

The authentication method which I've chosen was the Email/password authentication method for its method is easy to integrate all I had to do was copy the authentication script tag which onto my login HTML page  

Another implantation that I've taken into consideration in terms of authentication was allowing my system to give feedback end-users feedback such when account successfully created by blocking the user from accessing my database if their credentials are not matching in the user database  

How to again access the crud functions. 

Step one if you’re a new user you’re required to sign using your email and password (why email and password is required for that was my authentication method of choice) 

Step two once your account is verified, you’ll receive an alert message indicating that the account was successfully created  

Step 3 you sign in with your email and password 

And step four you'll have access to the data known as users where you can perform the CRUD functions. 