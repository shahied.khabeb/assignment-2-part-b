    const auth = firebase.auth()
     //Signup Function
  //Signup Function
  let signUpButton = document.getElementById('signup')
  signUpButton.addEventListener("click", (e) => {
    //Prevent Default Form Submission Behavior
    e.preventDefault()
    console.log("clicked")


    var email = document.getElementById("Email")
    var password = document.getElementById("Password")
    
    auth.createUserWithEmailAndPassword(email.value, password.value)
    .then((userCredential) => {
      location.reload();
      alert('Account created')
      // Signed in 
      var user = userCredential.user;
      console.log("user",user.email)
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log("error code", errorCode)
      alert('email-already-in-use')
    });
  })


  let signInButton = document.getElementById('signin')
  signInButton.addEventListener("click", (e) => {
    //Prevent Default Form Submission Behavior
    e.preventDefault()
    console.log("clicked")

    var email = document.getElementById("Email")
    var password = document.getElementById("Password")

    auth.signInWithEmailAndPassword(email.value, password.value) 
    .then((userCredential) => {
      // location.reload();
      // Signed in 
      var user = userCredential.user;
      console.log("user",user.email)
      window.location = "index.html";
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      // alert("error code", errorCode)
      alert( errorMessage)
    });
   })

   auth.onAuthStateChanged(function(user) {
    if (user) {
  
      var email = user.email
    
      var users = document.getElementById("user")
      var text = document.createTextNode(email);
  
      users.appendChild(text);
  
      console.log(users)
      //is signed in
    } else {
      //no user signed in
    }
  })
  


